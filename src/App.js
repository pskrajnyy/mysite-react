import React from 'react';
import Helmet from 'react-helmet';
import Layout from './components/layout/Layout';
import {metaText} from './texts/app';
import './scss/style.scss';

function App() {
  

  return (    
      <div>
        <Helmet>
          <title>{metaText.title}</title>
          <meta name="description" content={metaText.description} />
        </Helmet>        
        <Layout />
      </div>    
  );
}

export default App;