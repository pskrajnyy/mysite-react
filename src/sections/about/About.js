import React from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import styles from './About.module.scss';
import styled, { keyframes } from 'styled-components';
import AnimationContainer from '../../components/animationContainer/AnimationContainer';
import RevealContent from '../../components/revealContent/RevealContent';
import MeImage from '../../content/images/about/me.jpg';
import Tabs from '../../components/Tabs/Tabs';
import Counter from '../../components/counter/Counter';
import { FormattedMessage } from 'react-intl';

class About extends React.Component {

  shouldComponentUpdate() {
    return false;
  }

  render() {
    const AnimatedShadow = keyframes`
            0%   {box-shadow: 0 28px 60px rgba(4, 229, 229, .5);}
            10%  {box-shadow: 0 28px 60px rgba(243, 112, 85, .5);}
            20%  {box-shadow: 0 28px 60px rgba(239, 78, 123, .5);}
            30%  {box-shadow: 0 28px 60px rgba(161, 102, 171, .5);}
            40% {box-shadow: 0 28px 60px rgba(80, 115, 184, .5);}
            50% {box-shadow: 0 28px 60px rgba(4, 229, 229, .5);}
            60% {box-shadow: 0 28px 60px rgba(7, 179, 155, .5);}
            70% {box-shadow: 0 28px 60px rgba(111, 186, 130, .5);}
            80% {box-shadow: 0 28px 60px rgba(80, 115, 184, .5);}
            90% {box-shadow: 0 28px 60px rgba(16, 152, 173, .5);}
            100% {box-shadow: 0 28px 60px rgba(243, 112, 85, .5);}
        `;
    const ImageContainer = styled.div`
            border-radius: 20px;
            overflow: hidden;
            animation: ${AnimatedShadow} 10s infinite alternate;
            @media (max-width: 767px) {
                margin-bottom: 50px;
            }
        `;

    return(
      <section id='about' className={styles.aboutSection}>
        <Container className={styles.aboutContainer}>
          <Row>
            <Col className={styles.aboutLeftCol} md={6}>
              <AnimationContainer animation='fadeIn'>
                <ImageContainer>
                  <RevealContent delay={500}>
                    <img className={styles.imageAbout} src={MeImage} alt="about" />
                  </RevealContent>
                </ImageContainer>
              </AnimationContainer>
            </Col>
            <Col md={6}>
              <AnimationContainer animation='fadeIn'>
                <h2 id='aboutTitle' className={styles.aboutHeading}><FormattedMessage id='aboutSectionTitle' /></h2>
                <div className={styles.aboutSeparator} />
                <p className={styles.aboutText}><FormattedMessage id='aboutSectionDescription' /></p>
                <Tabs />
              </AnimationContainer>
            </Col>
          </Row>
        </Container>
        <Row className={styles.aboutCounterRow}>
          <Container>
            <Row>
              <Col md={4}>
                <AnimationContainer animation='fadeIn' delay={1000}>
                  <div className={styles.aboutCounterComponent}>
                    <Counter value={20} duration={5} delay={1000} symbol="+" text={<FormattedMessage id='counterFirst' />} animation={true} />
                  </div>
                </AnimationContainer>
              </Col>
              <Col md={4}>
                <AnimationContainer animation='fadeIn' delay={1000}>
                  <div className={styles.aboutCounterComponent}>
                    <Counter value={4} duration={5} delay={1000} symbol="+" text={<FormattedMessage id='counterSecond' />} animation={true} />
                  </div>
                </AnimationContainer>
              </Col>
              <Col md={4}>
                <AnimationContainer animation='fadeIn' delay={1000}>
                  <div className={styles.aboutCounterComponent}>
                    <Counter value={10} duration={5} delay={1000} symbol="+" text={<FormattedMessage id='counterThird' />} animation={true} />
                  </div>
                </AnimationContainer>
              </Col>
            </Row>
          </Container>
        </Row>
      </section>
    );
  }
}

export default About;
