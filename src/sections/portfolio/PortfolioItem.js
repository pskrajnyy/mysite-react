import React from 'react';
import RevealContent from '../../components/revealContent/RevealContent';
import styled, { keyframes } from 'styled-components';
import styles from './PortfolioItem.module.scss';
import Tilt from 'react-tilt';
import DesktopContent from './DesktopContent.js';

class PortfolioItem extends React.Component {
  constructor(props) {
    super(props);
    this.showContent = this.showContent.bind(this);
  }

  showContent() {
    setTimeout(() => {
      this.child.enable();
      document.getElementById(`portfolio-item-${this.props.index}`).classList.add('blue-shadow');
    }, 800);
  }

  showImage() {
    if(this.props.type === 'slider') {
      return <img className='portfolioItemImage' src={this.props.image} alt={this.props.text} />;
    } else {
      return (
        <RevealContent callParentMethod={true} parentMethod={this.showContent}>
          <img className={styles.portfolioItemImage} src={this.props.image} alt={this.props.text} />
        </RevealContent>
      );
    }
  }

  render() {
    const MoveUp = keyframes`
      0% { transform: translateY(0); }
      100% { transform: translateY(-20px); }
    `;

    const MoveDown = keyframes`
      0% { transform: translateY(0); }
      100% { transform: translateY(20px); }
    `;

    const Text = styled.div`
      transform: translateY(50px);
      transition: .5s;
      width: 100%;
      text-align: center;
    `;

    const MobileContent = styled.div`
      position: absolute;
      height: 100%;
      width: 100%;
      top: 0;
      opacity: 0 !important;
      transition: .5s;
      display: flex;
      align-items: flex-end;
      visibility: visible;
      background-image: linear-gradient(to top, rgba(4,229,229,1), rgba(255, 255, 255, 0));
      @media (min-width:1025px) {
        display: none !important;
      }
    `;    

    const Item = styled.div`
      position: relative;
      min-height: 400px;
      max-height: 400px;
      overflow: hidden;
      max-width: 95%;
      margin: 40px ${this.props.type !== 'slider' ? '0' : 'auto'};
      border-radius: 10px;
      &.move-up {
        animation: ${MoveUp} 5s infinite alternate;
      }
      &.move-down {
        animation: ${MoveDown} 5s infinite alternate;
      }
      &:hover {
        ${Text} {
          transform: translateY(-10px);
        }
        img {
          transform: scale(1.1);
        }
        ${MobileContent} {
          opacity: 1 !important;
        }
      }
      &.blue-shadow {
        box-shadow: 0 28px 60px rgb(4,229,229,.2);
        transition: .5s;
        &:hover {
          box-shadow: 0 28px 60px rgb(4,229,229,.5);
        }
      }
      `;    
        
      return (
        <Tilt className={styles.tilt} options={{ scale: 1, max: 10 }}>
          <Item className={`${this.props.index % 2 === 0 ? 'move-up' : 'move-down'}`} id={`portfolio-item-${this.props.index}`}>
            {this.showImage()}
            <MobileContent>
              <Text>
                <h4 className={styles.heading}>{this.props.text}</h4>
                <h5 className={styles.subHeading}>{this.props.category}</h5>
              </Text>
            </MobileContent>
            <DesktopContent text={this.props.text} category={this.props.category} link={this.props.link} live={this.props.live} ref={(cd) => this.child = cd} type={this.props.type} />
          </Item>
        </Tilt>
      );        
    }
  }
    
  export default PortfolioItem;
    