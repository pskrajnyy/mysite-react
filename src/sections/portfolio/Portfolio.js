import React from "react";
import {Col} from 'react-bootstrap';
import styles from './Portfolio.module.scss';
import styled, { keyframes } from 'styled-components';
import PortfolioItem from './PortfolioItem.js';
import Slider from "react-slick";
import AnimatedHeading from '../../components/animationHeading/AnimationHeading';
import {FormattedMessage} from 'react-intl';
import data from '../../content/portfolio/data.json';

const portfolioSlick = {
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  dots: true,
  draggable: true, 
  focusOnSelect: false,
  autoplay: true,
  arrows: false,
  responsive: [{
      breakpoint: 800,
      settings: {
          slidesToShow: 3,
      }
  },
  {
      breakpoint: 1200,
      settings: {
          slidesToShow: 3,
      }
  },
  {
      breakpoint: 993,
      settings: {
          slidesToShow: 2,
      }
  },
  {
      breakpoint: 769,
      settings: {
          slidesToShow: 2,
      }
  },
  {
      breakpoint: 481,
      settings: {
          slidesToShow: 1,
      }
  }
]
};

class Portfolio extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: 'all',
    };
  }

  render() {
    const ColorAnimation = keyframes`
            0%  {border-color: #04e5e5;}
            10% {border-color: #f37055;}
            20% {border-color: #ef4e7b;}
            30% {border-color: #a166ab;}
            40% {border-color: #5073b8;}
            50% {border-color: #04e5e5;}
            60% {border-color: #07b39b;}
            70% {border-color: #6fba82;}
            80% {border-color: #5073b8;}
            90% {border-color: #1098ad;}
            100% {border-color: #f37055;}
        `;

    const TabSelector = styled.button`
            color: #fff;
            font-size: 20px;
            font-weight: bold;
            background-color: transparent;
            border: none;
            margin: 0 10px 0 0;
            border-bottom: 2px solid #fff;
            transition: .5s;
            &:hover, &.active {
                animation: ${ColorAnimation} 10s infinite alternate;
            }
            &:focus {
                outline: none;
            }
            @media (max-width: 767px) {
                font-size: 18px;
            }
        `;

    return(
        <section id='portfolio' className={styles.portfolioSection}>
            <Col md={12}>
                
                    <FormattedMessage id='portfolioSectionTitle'>
                        {(msg) => <AnimatedHeading text={msg} />}
                    </FormattedMessage>            
                    <div className={styles.portfolioTabSelectors}>
                    <TabSelector className={this.state.tab === 'all' ? 'active' : ''} onClick={() => this.setState({tab: 'all'})}>
                        <FormattedMessage id='portfolioTabAll' />
                    </TabSelector>
                    <TabSelector className={this.state.tab === 'java' ? 'active' : ''} onClick={() => this.setState({tab: 'java'})}>
                        <FormattedMessage id='portfolioTabJava' />
                    </TabSelector>
                    <TabSelector className={this.state.tab === 'html' ? 'active' : ''} onClick={() => this.setState({tab: 'html'})}>
                        <FormattedMessage id='portfolioTabHtml' />
                    </TabSelector>
                    <TabSelector className={this.state.tab === 'react' ? 'active' : ''} onClick={() => this.setState({tab: 'react'})}>
                        <FormattedMessage id='portfolioTabReact' />
                    </TabSelector>
                    </div>
                    <div className={styles.portfolioSlickActivation}>
                        <Slider {...portfolioSlick}>
                            {this.portfolio(this.state.tab)}
                        </Slider>
                    </div>
                
            </Col>
        </section>
    );
  }

    portfolio(selector) {
        const items = data.Portfolio;
        let shuffleItems = items;
        if(selector !== 'all') {
            shuffleItems = this.shuffle(items.filter(item => item.category.toLowerCase().includes(selector)));
        } else {
            shuffleItems = this.shuffle(items);
        }
        return shuffleItems.map((value, index) => {
            return (
              <div key={value.id}>
                <PortfolioItem
                  index={value.id}
                  image={value.image}
                  text={value.title}
                  category={value.category}
                  link={value.link}
                  live={value.live}
                  type="col"
                />
              </div>
            );
          });      
    }
    
    shuffle(items) {
        for (var i = items.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = items[i];
            items[i] = items[j];
            items[j] = temp;
        }
        return items;
    }
}

export default Portfolio;