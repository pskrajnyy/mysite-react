import React from 'react';
import styles from './ContactForm.module.scss';
import styled, { keyframes } from 'styled-components';
import * as emailjs from 'emailjs-com';
import {FormattedMessage} from 'react-intl';

class ContactForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      phone: '',
      message: '',
      error: false,
    };
  }

  formSubmit() {
    if (this.state.name === '' || this.state.email === '' || this.state.message === '') {
      this.setState({error: true});
    } else {
      this.setState({error: false});
      const {name, email, phone, message} = this.state;

      let templateParams = {
        from_name: name,
        from_email: email,
        from_phone: phone,
        to_name: 'gmail',
        message_html: message,
      }

      emailjs.send(
        'gmail',
        'template_Pht8BJRZ',
        templateParams,
        'user_fXBSTEmjB4HVsjNa5ggFx'
      )
     
     this.resetForm();
    }
    
    this.forceUpdate();
  }

  resetForm() {
    this.setState({name:'', email:'', phone: '', message: ''})
    document.getElementById('textareaform').value = '';
  }

  check(val) {
    if (this.state.error && val === '') {
      return false;
    } else {
      return true;
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }

  render() {
    const ErrorInputAnimation = keyframes`
            0% {
              border-bottom: 1px solid #111;
            }
            100% {
              border-bottom: 1px solid #ff0000;
            }
        `;


    const Input = styled.input`
            width: 100%;
            background-color: #111;
            border: none;
            border-bottom: 1px solid #444;
            padding: 10px 5px;
            border-radius: 0;
            color: #fff;
            transition: .5s;
            &:focus {
              border-bottom: 1px solid #04e5e5;
              outline: none;
            }
            &.error {
              animation: ${ErrorInputAnimation} 1s forwards;
            }
        `;

    return(
      <div className={styles.contactForm}>
        <h2 className={styles.heading}>
          <FormattedMessage id='formTitle' />
        </h2>
        <div className={styles.separator} />
        <div className={styles.inputElement}>
          <FormattedMessage id='placeholderName'>
            {(msg) => (<Input type="text" defaultValue={this.state.name}  className={`name ${this.check(this.state.name) ? '' : 'error'}`} placeholder={msg} onChange={e => this.setState({name: e.target.value})} />)}
          </FormattedMessage>
        </div>
        <div className={styles.inputElement}>
          <FormattedMessage id='placeholderEmail'>
            {(msg) => (<Input type="text" defaultValue={this.state.email} className={`email ${this.check(this.state.email) ? '' : 'error'}`} placeholder={msg} onChange={e => this.setState({email: e.target.value})} />)}
          </FormattedMessage>
        </div>
        <div className={styles.inputElement}>
          <FormattedMessage id='placeholderPhone'>
            {(msg) => (<Input type="text" defaultValue={this.state.phone} className="phone" placeholder={msg} onChange={e => this.setState({phone: e.target.value})} />)}
          </FormattedMessage>
        </div>
        <div className={styles.inputElement}>
          <FormattedMessage id='placeholderMessage'>
            {(msg) => (<textarea id='textareaform' placeholder={msg} defaultValue={this.state.message}  className={styles.textarea + ` message ${this.check(this.state.message) ? '' : 'error'}`} onChange={e => this.setState({message: e.target.value})} />)}
          </FormattedMessage>
        </div>
        <button className={styles.submitButton} onClick={() => this.formSubmit()}>
          <span>
            <FormattedMessage id='formSubmit' />
          </span>
        </button>
      </div>
    );
  }
}

export default ContactForm;
