import React from 'react';
import styles from './Contact.module.scss';
import { Row, Col, Container } from 'react-bootstrap';
import styled, { keyframes } from 'styled-components';
import AnimationContainer from '../../components/animationContainer/AnimationContainer';
import ContactForm from './ContactForm';
import MailImage from '../../content/images/icons/email.png';
import MapImage from '../../content/images/icons/map.png';
import PhoneImage from '../../content/images/icons/phone.png';
import {FormattedMessage} from 'react-intl';

class Contact extends React.Component {
  render() {
    const gradientAnimation = keyframes`
            0% {
                background-position: 15% 0%;
            }
            50% {
                background-position: 85% 100%;
            }
            100% {
                background-position: 15% 0%;
            }
        `;

    const Gradient = styled.div`
            position: absolute;
            height: 100%;
            width: 100%;
            top: 0;
            clip-path: polygon(0% 100%, 10px 100%, 10px 10px, calc(100% - 10px) 10px, calc(100% - 10px) calc(100% - 10px), 10px calc(100% - 10px), 10px 100%, 100% 100%, 100% 0%, 0% 0%);
            background: linear-gradient(120deg, #04e5e5, #f37055, #ef4e7b, #a166ab, #5073b8, #1098ad, #07b39b, #6fba82);
            background-size: 300% 300%;
            animation: ${gradientAnimation} 5s ease-in-out infinite;
        `;

    const IconContainer = styled.div`
            width: 150px;
            height: 150px;
            margin: auto;
            padding: 35px;
            text-align: center;
            position: relative;
            bottom: 75px;
            background: linear-gradient(120deg, #04e5e5, #f37055, #ef4e7b, #a166ab, #5073b8, #1098ad, #07b39b, #6fba82);
            background-size: 300% 300%;
            animation: ${gradientAnimation} 5s ease-in-out infinite;
            border-radius: 150px;
            transition: .5s;
        `;

    return(
      <section className={styles.sectionContact} id='contact'>
        <Container>
          <AnimationContainer animation='fadeIn'>
            <Row>
              <Col className={styles.contactCol} md={8}>
                <ContactForm />
                <Gradient />
              </Col>
            </Row>
          </AnimationContainer>
          <Row className={styles.iconRow}>
            <Col className={styles.iconCol} md={4}>
              <AnimationContainer animation="fadeIn" delay={500}>
                <div className={styles.infoPart}>
                  <IconContainer>
                    <img className={styles.icon} src={MailImage} alt="email" />
                  </IconContainer>
                  <div className={styles.infoContainer}>
                    <h4 className={styles.infoTitle}>
                      <FormattedMessage id='emailTitle' />
                    </h4>
                    <div className={styles.infoLinkContainer}>
                      <a className={styles.infoLink} href="mailto:email@yoursite.com">
                        <FormattedMessage id='emailDescription' />
                      </a>
                    </div>
                  </div>
                </div>
              </AnimationContainer>
            </Col>
            <Col className={styles.iconCol} md={4}>
              <AnimationContainer animation="fadeIn" delay={1000}>
                <div className={styles.infoPart}>
                  <IconContainer>
                    <img className={styles.icon} src={PhoneImage} alt="phone" />
                  </IconContainer>
                  <div className={styles.infoContainer}>
                    <h4 className={styles.infoTitle}>
                      <FormattedMessage id='phoneTitle' />
                    </h4>
                    <div className={styles.infoLinkContainer}>
                      <a className={styles.infoLink} href="tel:+660247705">
                        <FormattedMessage id='phoneDescription' />
                      </a>
                    </div>
                  </div>
                </div>
              </AnimationContainer>
            </Col>
            <Col className={styles.iconCol} md={4}>
              <AnimationContainer animation="fadeIn" delay={1500}>
                <div className={styles.infoPart}>
                  <IconContainer>
                    <img className={styles.icon} src={MapImage} alt="map" />
                  </IconContainer>
                  <div className={styles.infoContainer}>
                    <h4 className={styles.infoTitle}>
                      <FormattedMessage id='mapTitle' />
                    </h4>
                    <div className={styles.infoLinkContainer}>
                      <a className={styles.infoLink} target="_blank" rel="noopener noreferrer" href="https://www.google.com/maps/place/Gdynia,+Poland/@54.5039429,18.3230526,11z/data=!3m1!4b1!4m5!3m4!1s0x46fda145071ed789:0xdee2f99989236636!8m2!3d54.5188898!4d18.5305409?hl=en">
                        <FormattedMessage id='mapDescription' />
                      </a>
                    </div>
                  </div>
                </div>
              </AnimationContainer>
            </Col>
          </Row>
        </Container>
      </section>
    );
  }
}

export default Contact;


