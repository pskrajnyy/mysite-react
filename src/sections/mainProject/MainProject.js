import React from 'react'
import { Container } from 'react-bootstrap'
import styled from 'styled-components'
import AnimationContainer from '../../components/animationContainer/AnimationContainer';
import MainProjectContent from './MainProjectContent';
import AnimatedHeading from '../../components/animationHeading/AnimationHeading';
import {FormattedMessage} from 'react-intl';

class MainProject extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            active: 0
        }
    }
    render() {
        const Section = styled.section`
            position: relative;
            overflow: hidden;
            background-color: #000;
            background-size: cover;
        `

        const TestimonialContainer = styled.div`
            padding: 100px 0;
            @media (max-width: 767px) {
                padding: 50px 10px;
            }
        `

        return(
            <Section id="mainProject">
                <TestimonialContainer>
                    <Container>
                        <FormattedMessage id='mainProjectSectionTitle'>
                            {(msg) => <AnimatedHeading text={msg} />}
                        </FormattedMessage>       
                        <AnimationContainer animation="fadeIn">
                            <MainProjectContent />
                        </AnimationContainer>
                    </Container>
                </TestimonialContainer>
            </Section>
        )
    }
}
export default MainProject;