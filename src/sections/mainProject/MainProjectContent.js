import React from 'react'
import { Row, Col, Container } from 'react-bootstrap'
import styled, { keyframes } from 'styled-components'
import BaffleText from '../../components/baffleText/BaffleText'
import {faChrome} from '@fortawesome/free-brands-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {FormattedMessage} from 'react-intl';


class MainProjectContent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            active: 0
        }
    }
    render() {

        return(
            <Container>
                {this.testimonials()}
            </Container>
        )
    }

    testimonials() {
        const gradientAnimation = keyframes`
            0% {
                background-position: 15% 0%;
            }
            50% {
                background-position: 85% 100%;
            }
            100% {
                background-position: 15% 0%;
            }
        `

        const Gradient = styled.div`
            position: absolute;
            height: 100%;
            width: 100%;
            top: 0;
            background: linear-gradient(120deg, #04e5e5, #f37055, #ef4e7b, #a166ab, #5073b8, #1098ad, #07b39b, #6fba82);
            background-size: 300% 300%;
            clip-path: polygon(0% 100%, 10px 100%, 10px 10px, calc(100% - 10px) 10px, calc(100% - 10px) calc(100% - 10px), 10px calc(100% - 10px), 10px 100%, 100% 100%, 100% 0%, 0% 0%);
            animation: ${gradientAnimation} 5s ease-in-out infinite;
        `

        const TestimonialBox = styled.div`
            min-height: 500px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            position: relative;
            @media (max-width:767px) {
                min-height: 400px;
                padding: 0;
            }
        `

        const ImageBox = styled.div`
            background-color: #fff;
            min-height: 500px;
            max-height: 500px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            position: relative;
            overflow: hidden;
            @media (max-width:767px) {
                min-height: 400px;
                padding: 0;
            }
        `

        const TestimonialCol = styled(Col)`
            padding: 0;
        `

        const Image = styled.img`
            height: 100%;
            width: 100%;
            object-fit: cover;
            @media (min-width:768px) {
                min-height: 500px;
                max-height: 500px;
            }
            @media (max-width:768px) {
                min-height: 400px;
                max-height: 400px;
            }
        `

        const Fade = keyframes`
            0% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        `

        const TestimonialRow = styled(Row)`
            animation: ${Fade} 1s forwards;
            margin-top: 50px;
        `
        
        const TestimonialText = styled.div`
            color: #eee;
            line-height: 25px;
            max-width: 80%;
            margin: 0 auto 10px auto;
            text-align: center;
            font-size: 50px;
            span {
                font-size: 16px;
                @media (max-width: 500px) {
                    font-size: 14px;
                    line-height: 20px;
                }
            }
        `

        const TestimonialClient = styled.div`
            display: flex;
            align-items: center;
            justify-content: center;
            margin: 40px 20px;
        `
        
        const TestimonialClientInfo = styled.div`
            display: flex;
            justify-content: center;
            flex-direction: column;
            text-align: center;
        `

        const TestimonialClientName = styled.p`
            color: #eee;
            margin-bottom: 0;
            font-weight: 600;
            font-size: 20px;
        `

        const Icons =styled.div`
            text-align:center;
            font-size:20px;
            .social_icon {
                margin: 5px 10px;
            }
            .social_icon:hover {
                color: #fff;
                cursor: pointer;
            }
        `

        return (
        <TestimonialRow key={1}>
            <TestimonialCol md={6}>
                <ImageBox>
                    <Image src={'portfolio/tacholider.jpg'} alt={'tacholider'} />
                </ImageBox>
            </TestimonialCol>
            <TestimonialCol md={6}>
                <TestimonialBox>
                    <TestimonialText>
                        {'Tacholider.eu'}
                    </TestimonialText>
                    <TestimonialClient>
                        <TestimonialClientInfo>
                            <TestimonialClientName>
                                <BaffleText
                                    text={<FormattedMessage id='mainProjectDescription' />}
                                    revealDuration={1000}
                                    revealDelay={0}
                                    characters="AaBbCcDeEeFfGgHhIiJjKkLlMmNnOpPpQqRrSsTtUuVvWwXxYyZ"
                                />
                            </TestimonialClientName>
                        </TestimonialClientInfo>
                    </TestimonialClient>
                    <Icons>
                        <FontAwesomeIcon icon={faChrome} className="social_icon" onClick={() => window.open('https://tacholider.eu')}/>
                    </Icons>
                    <Gradient />
                </TestimonialBox>
            </TestimonialCol>
        </TestimonialRow>
    )}
}

export default MainProjectContent;