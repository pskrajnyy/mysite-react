import React from 'react';
import styles from './Skills.module.scss';
import { Row, Col, Container } from 'react-bootstrap';
import styled, { keyframes } from 'styled-components';
import AnimationContainer from '../../components/animationContainer/AnimationContainer';
import AnimationHeading from '../../components/animationHeading/AnimationHeading';
import JavaImage from '../../content/images/skills/java.png';
import HibernateImage from '../../content/images/skills/hibernate.png';
import SpringImage from '../../content/images/skills/spring.png';
import HtmlImage from '../../content/images/skills/html.png';
import CssImage from '../../content/images/skills/css.png';
import SassImage from '../../content/images/skills/sass.png';
import JavascriptImage from '../../content/images/skills/javascript.png';
import ReactImage from '../../content/images/skills/react.png';
import ReduxImage from '../../content/images/skills/redux.png';
import BootstrapImage from '../../content/images/skills/bootstrap.png';
import NodejsImage from '../../content/images/skills/node.png';
import WebpackImage from '../../content/images/skills/webpack.png';
import ExtjsImage from '../../content/images/skills/ext.png';
import GitImage from '../../content/images/skills/git.png';
import MongoImage from '../../content/images/skills/mongo.png';
import MyBatisImage from '../../content/images/skills/mybatis.png';
import PostgreSQL from '../../content/images/skills/postgresql.png';
import MySql from '../../content/images/skills/mysql.png';
import { FormattedMessage } from 'react-intl';

class Skills extends React.Component {

  shouldComponentUpdate() {
    return false;
  }

  render() {
    const gradientAnimation = keyframes`
            0% {
                background-position: 15% 0%;
            }
            50% {
                background-position: 85% 100%;
            }
            100% {
                background-position: 15% 0%;
            }
        `;

    const SkillsRow = styled(Row)`
            justify-content: center;
            border-radius: 20px;
            background: linear-gradient(120deg, #04e5e5, #f37055, #ef4e7b, #a166ab, #5073b8, #1098ad, #07b39b, #6fba82);
            background-size: 300% 300%;
            animation: ${gradientAnimation} 5s ease-in-out infinite;
        `;

    const BackgroundColor = keyframes`
          0%  {background-color: #04e5e5;}
          10% {background-color: #f37055;}
          20% {background-color: #ef4e7b;}
          30% {background-color: #a166ab;}
          40% {background-color: #5073b8;}
          50% {background-color: #04e5e5;}
          60% {background-color: #07b39b;}
          70% {background-color: #6fba82;}
          80% {background-color: #5073b8;}
          90% {background-color: #1098ad;}
          100% {background-color: #f37055;}
      `;
    const SkillCol = styled(Col)`
          text-align: center;
          padding: 20px 5px;
          transition: .1s;
          &:hover {
            transform: scale(1.1);
            animation: ${BackgroundColor} 10s infinite alternate;
            z-index: 5;
            border-radius: 10px;
          }
      `;

    const Skill = styled.img`
          height: 100px;
      `;

    return (
      <section className={styles.skillsSection} id='skills'>
        <FormattedMessage id='skillsSectionTitle'>
          {(msg) => <AnimationHeading className={styles.titleSection} text={msg} />}
        </FormattedMessage>        
        <Container className={styles.skillsContainer}>
          <Row>
            <Col lg={12}>
              <SkillsRow>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={1*200}>
                    <Skill src={JavaImage} alt="Skill" />
                    <p className={styles.skillDescription}>JAVA</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={2*200}>
                    <Skill src={SpringImage} alt="Skill" />
                    <p className={styles.skillDescription}>Spring Boot</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={3*200}>
                    <Skill src={HibernateImage} alt="Skill" />
                    <p className={styles.skillDescription}>Hibernate</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={1*200}>
                    <Skill src={MyBatisImage} alt="Skill" />
                    <p className={styles.skillDescription}>MyBatis</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={1*200}>
                    <Skill src={PostgreSQL} alt="Skill" />
                    <p className={styles.skillDescription}>PostgreSQL</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={1*200}>
                    <Skill src={MySql} alt="Skill" />
                    <p className={styles.skillDescription}>MySQL</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={1*200}>
                    <Skill src={MongoImage} alt="Skill" />
                    <p className={styles.skillDescription}>MongoDB</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={4*200}>
                    <Skill src={HtmlImage} alt="Skill" />
                    <p className={styles.skillDescription}>HTML</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={5*200}>
                    <Skill src={CssImage} alt="Skill" />
                    <p className={styles.skillDescription}>CSS</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={6*200}>
                    <Skill src={SassImage} alt="Skill" />
                    <p className={styles.skillDescription}>Sass</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={7*200}>
                    <Skill src={JavascriptImage} alt="Skill" />
                    <p className={styles.skillDescription}>JavaScript</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={8*200}>
                    <Skill src={BootstrapImage} alt="Skill" />
                    <p className={styles.skillDescription}>Bootstrap</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={7*200}>
                    <Skill src={ReactImage} alt="Skill" />
                    <p className={styles.skillDescription}>React.JS</p>
                  </AnimationContainer>
                </SkillCol> 
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={6*200}>
                    <Skill src={ReduxImage} alt="Skill" />
                    <p className={styles.skillDescription}>Redux</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={5*200}>
                    <Skill src={NodejsImage} alt="Skill" />
                    <p className={styles.skillDescription}>Node.JS</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={4*200}>
                    <Skill src={WebpackImage} alt="Skill" />
                    <p className={styles.skillDescription}>Webpack</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={3*200}>
                    <Skill src={ExtjsImage} alt="Skill" />
                    <p className={styles.skillDescription}>Ext.JS</p>
                  </AnimationContainer>
                </SkillCol>
                <SkillCol lg={2} md={3} sm={4} xs={6}>
                  <AnimationContainer animation="fadeIn slower" delay={2*200}>
                    <Skill src={GitImage} alt="Skill" />
                    <p className={styles.skillDescription}>GIT</p>
                  </AnimationContainer>
                </SkillCol>
              </SkillsRow>
            </Col>
          </Row>
        </Container>
      </section>
    );
  }
}

export default Skills;
