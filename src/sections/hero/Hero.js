import React from 'react';
import styles from './Hero.module.scss';
import styled, { keyframes } from 'styled-components';
import LoopVideo from './assets/loop.mp4';
import cvPL from './assets/cvPL.pdf';
import Glitch from '../../components/glitch/Glitch';
import Typewriter from 'typewriter-effect';
import { FormattedMessage } from 'react-intl';

class Hero extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      height: 0,
      width: 0,
    };
  }

  updateDimensions = () => {
    if (this.state.height !== window.innerHeight) {
      this.setState({height: window.innerHeight});
    }
    if (this.state.width !== window.innerWidth) {
      this.setState({width: window.innerWidth});
    }
  }

  componentDidMount() {   
    this.checkLanguage(); 
    this.setState({height: window.innerHeight, width: window.innerWidth});
    window.addEventListener('resize', this.updateDimensions);
    document.body.addEventListener('mousemove', (e) => {
      var amountMovedX = (e.clientX * -.1 / 8);
      var amountMovedY = (e.clientY * -.1 / 8);
      var x = document.getElementsByClassName('parallax-hero-item');
      var i;
      for (i = 0; i < x.length; i++) {
        x[i].style.transform='translate(' + amountMovedX + 'px,' + amountMovedY + 'px)';
      }
    });
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  checkLanguage() {
    const mainPageNavbar = document.getElementById('homeNavbar');
    if(mainPageNavbar) {
      console.log(mainPageNavbar.innerHTML);
      if(mainPageNavbar.innerHTML === 'Strona główna') {
        return false;
      } else {
        return true;
      }
    }
  }

  render() {
    const gradientAnimation = keyframes`
            0% {
              background-position: 15% 0%;
            }
            50% {
              background-position: 85% 100%;
            }
            100% {
              background-position: 15% 0%;
            }
          `;
    const HeadingBox = styled.div`
            height: 550px;
            width: 1000px;
            margin: auto;
            position: relative;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            &:after {
                content: '';
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                border-radius: 5px;
                background: linear-gradient(120deg, #04e5e5, #f37055, #ef4e7b, #a166ab, #5073b8, #1098ad, #07b39b, #6fba82);
                background-size: 300% 300%;
                clip-path: polygon(0% 100%, 10px 100%, 10px 10px, calc(100% - 10px) 10px, calc(100% - 10px) calc(100% - 10px), 10px calc(100% - 10px), 10px 100%, 100% 100%, 100% 0%, 0% 0%);
            }
            &.animate:after {
                animation: ${gradientAnimation} 2s ease-in-out infinite;
            }
            @media (max-width: 1050px) {
                width: 90%;
                height: 475px;
            }
            @media (max-width: 650px) {
              height: 420px; 
            }
            @media (max-width: 500px) {
              height: 360px; 
            }
            @media (max-height: 690px) {
              height: 70%;
            }
        `;    

    return(
      <section id='home' className={styles.hero}>
        <div className={styles.videoContainer} style={{height: `${this.state.height}px`}}>
          <video autoPlay="autoplay" loop="loop" muted style={{height: `${this.state.width >= 1024 && this.state.width < 1200 ? '100%': 'auto'}`}}>
            <source src={LoopVideo} type="video/mp4" />
          </video>
          <HeadingBox className='parallax-hero-item animate'>
            <h2 className={styles.subHeading + ' parallax-hero-item'}><FormattedMessage id='hello' /></h2>
            <div className='parallax-hero-item'>
              <Glitch text="Przemysław Skrajny" />
            </div>
            <div className={styles.type + ' parallax-hero-item'}>
              <Typewriter options={{
                strings: [
                  'Java Developer',
                  'Web Developer',
                  'Full stack Developer',
                ],
                autoStart: true,
                loop: true,
              }}
              />
            </div>
            <a id='cvDownloadButton' className={styles.resume} style={{display: `${this.checkLanguage() ? 'none': 'display'}`}} href={cvPL}><FormattedMessage id='downloadCv' /></a>
          </HeadingBox>
        </div>
      </section>
    );
  }
}

export default Hero;
