import React, {useState} from 'react';
import Index from '../pageRevealer/PageRevealer';
import Hero from '../../sections/hero/Hero';
import Navbar from '../../components/navbar/Navbar';
import Skills from '../../sections/skills/Skills';
import Contact from '../../sections/contact/Contact';
import About from '../../sections/about/About';
import MainProject from '../../sections/mainProject/MainProject';
import Portfolio from '../../sections/portfolio/Portfolio';
import Footer from '../footer/Footer';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../scss/style.scss';
import {IntlProvider} from 'react-intl';
import messages_en from '../../translations/en.json';
import messages_pl from '../../translations/pl.json';

function Layout({children}) {
  const messages = {
    'en': messages_en,
    'pl': messages_pl
  };

  const [locale, setLocale] = useState('pl');

  const handleSelect = e => {
    setLocale(e);
  };

  return (
    <div id="main">
      <Index />
      <IntlProvider locale={locale} messages={messages[locale]}>
        <div>
          <Navbar setLocale={handleSelect} scroll={true} />
          <Hero />
          <About />
          <Skills />
          <Portfolio />
          <Contact />
        </div>
      </IntlProvider>
      <Footer />
    </div>
  );
}

export default Layout;
