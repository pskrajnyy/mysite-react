import React from 'react';
import styled from 'styled-components';
import styles from './Navbar.module.scss';
import { Container } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { FormattedMessage } from 'react-intl';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ToggleButton from 'react-bootstrap/ToggleButton';
// import cvPL from '../../sections/hero/assets/cvPL.pdf';
// import cvEN from '../../sections/hero/assets/cvEN.pdf';
var scrollToElement = require('scroll-to-element');

class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
      sticky: false,
      sections: ['homeNavbar', 'aboutNavbar', 'skillsNavbar', 'mainProjectNavbar', 'portfolioNavbar', 'contactNavbar'],
      language: 'pl'
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll, { passive: true });
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (window.pageYOffset >= 50 && this.state.sticky) {
      return this.state.collapse !== nextState.collapse;
    } else {
      return true;
    }
  }

  handleScroll = event => {
    if (window.pageYOffset >= 1) {
      this.setState({ sticky: true });
    } else {
      this.setState({ sticky: false });
    }
  }

  collapseNav() {
    console.log(this.state, 'col');
    if (!this.state.collapse) {
      this.setState({ collapse: true });
    } else {
      this.setState({ collapse: false });
    }
  }

  changeLanguage(language) {
    this.setState({language: language});
    this.props.setLocale(language);
  }

  render() {
    const NavbarWrapper = styled.div`
            position: absolute;
            z-index: 1;
            width: 100%;
            padding: 20px 0;
            z-index: 99;
            &.sticky {
                position: fixed;
                background-color: rgba(0,0,0,.8);
                padding: 0 0;
                .btn-group {
                  display: none !important;
                }
            }
        `;

    const NavbarContainer = styled(Container)`
            display: flex;
            position: relative;
            @media (max-width: 576px) {
                display: block;
                padding: 0 20px;
            }

        `;

    const Nav = styled.nav`
            flex: 100%;
            max-width: 100%;
            display: flex;
            justify-content: flex-end;
            align-items: center;
            @media (max-width: 576px) {
                flex: 0 0 100%;
                max-width: 100%;
                justify-content: center;
                background-color: rgba(0,0,0,.8);
                margin-top: 20px;
                &.hidden_mobile {
                    display: none;
                }
            }
        `;

    const NavInner = styled.div`
            justify-content: flex-end;
        `;

    const Toggler = styled.button`
            color: #fff;
            position: absolute;
            right: 0;
            top: 0;
            @media (min-width: 576px) {
              display: none;
            }
        `;

    return(
      <NavbarWrapper className={`header${this.state.sticky === true ? ' sticky' : ''}`}>
        <NavbarContainer>
          <Toggler onClick={() => this.collapseNav()} className={styles.bars + ' navbar-toggler navbar-toggler-right'}>
            <FontAwesomeIcon icon={faBars} className="bars" />
          </Toggler>
          <ButtonGroup id='localButtons' className={styles.toggleGroup} toggle>
            <ToggleButton className={styles.toggleButton} key={0} type='radio' variant='outline-dark' name='PL' checked={this.state.language === 'pl'} value={0} onChange={() => this.changeLanguage('pl')}>PL</ToggleButton>
            <ToggleButton className={styles.toggleButton} key={1} type='radio' variant='outline-dark' name='EN' checked={this.state.language === 'en'} value={1} onChange={() => this.changeLanguage('en')}>EN</ToggleButton>
          </ButtonGroup>
          <Nav className={`navbar navbar-expand-sm ${this.state.collapse === true ? 'expand' : 'hidden_mobile'}`}>
            <NavInner className={`navbar-collapse collapse ${this.state.collapse === true ? 'show' : ''}`}>
              <div className="navbar-nav">{this.navItems()}</div>
            </NavInner>
          </Nav>
        </NavbarContainer>
      </NavbarWrapper>
    );
  }

  navigate(id) {
    if (this.props.scroll) {
      const el = document.getElementById(id);
      this.setState({collapse: false});
      scrollToElement(el, {
        offset: 0,
        ease: 'in-out-expo',
        duration: 2000,
      });
    } else {
      window.location.href = `./#${id}`;
    }
  }

  navItems() {
    const NavItem = styled.button`
            background: none;
            border: none;
            color: #fff;
            text-transform: capitalize;
            font-weight: 500;
            margin: 10px 5px;
            transition: .5s;
            &:hover {
                color: #1098ad;
            }
            &:focus {
                outline: none;
            }
            @media (min-width: 501px) and (max-width: 767px) {
                font-size: 11px;
                margin: 2px;
            }
        `;

    return this.state.sections.map((value, index) => {
      return (
        <NavItem  id={value} key={index} onClick={() => this.navigate(value.substr(0, value.length - 6))}>
          <FormattedMessage id={value} />
        </NavItem>
      );
    });
  }
}

export default Navbar;
