import React from 'react';
import styles from './Timeline.module.scss';

class Timeline extends React.Component {

  render() {
    const items = [];
    let {data} = this.props;
    for(const object of data) {
      items.push(
        <div key={object.id} className={styles.timelineContainer}>
          <div className={styles.timelineDetails}>
            <h5 className={styles.timelineYears}>{object.year}</h5>
            <h4 className={styles.timelineHeading}>{object.title}</h4>
            <h5 className={styles.timelineCompany}>{object.institution}</h5>
            <p className={styles.timelineText}>{object.description}</p>
          </div>
        </div>);
    }
    return(
      <div>
        {items}
      </div>
    );
  }
}

export default Timeline;
