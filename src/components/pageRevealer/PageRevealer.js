import React from 'react';
import styles from './PageRevealer.module.scss';
import Index from '../baffleText/BaffleText';
import AnimationContainer from '../animationContainer/AnimationContainer';
import styled, { keyframes } from 'styled-components';

class PageRevealer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      animation : false,
      complete: false,
      hide: false,
    };
    this.reveal = this.reveal.bind(this);
  }

  reveal() {
    if (!this.state.complete) {
      this.setState({animation: true, complete: true});
      setTimeout(() => {
        document.getElementById('reveal_container').style.backgroundColor = 'transparent';
        setTimeout(() => {
          this.setState({animation: false, hide: true});
        }, 500);
      }, 400);
    }
  }

  baffle() {
    if (!this.state.complete) {
      return (
        <AnimationContainer animation="fadeIn" delay="10">
          <Index
            text="Przemysław Skrajny - Full stack Developer"
            revealDuration={500}
            revealDelay={1000}
            parentMethod={this.reveal}
            callMethodTime={3000}
          />
        </AnimationContainer>
      );
    }
  }

  render() {
    const RevealAnimation = keyframes`
            0% {
                transform: translate3d(100%, 0, 0);
            }
            35%, 65% {
                transform: translate3d(0, 0, 0);
            }
            100% {
                transform: translate3d(-100%, 0, 0);
            }
        `;
    const Reveal = styled.div`
            position: fixed;
            width: 100%;
            pointer-events: none;
            height: 100%;
            background-color: #04e5e5;
            transform: translateX(100%);
            &.animate {
                animation: ${RevealAnimation} 1.1s cubic-bezier(0.2, 1, 0.3, 1) forwards;
            }
        `;
    return (
      <div className={styles.revealContainer} id="reveal_container" style={{display: this.state.hide ? 'none' : 'flex'}}>
        {this.baffle()}
        <Reveal id="revealer" className={this.state.animation ? 'animate' : ''} />
      </div>
    );
  }
}

export default PageRevealer;
