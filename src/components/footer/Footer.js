import React from 'react';
import styles from './Footer.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGitlab, faLinkedin } from '@fortawesome/free-brands-svg-icons';

class Footer extends React.Component {
  render() {
    return (
      <div className={styles.footerMain}>
        <div>
          <FontAwesomeIcon icon={faGitlab} className={styles.socialIcon} onClick={() => window.open('https://gitlab.com/pskrajnyy')}/>
          <FontAwesomeIcon icon={faLinkedin} className={styles.socialIcon} onClick={() => window.open('https://www.linkedin.com/in/przemysław-skrajny-b48309181')} />
        </div>
      </div>
    );
  }
}

export default Footer;
