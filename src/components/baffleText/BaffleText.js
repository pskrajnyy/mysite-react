import React from 'react';
import Baffle from 'baffle-react';
import handleViewport from 'react-in-viewport';

class BaffleText extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      animation_complete: false,
      obfuscate: true,
      force: false,
    };
  }

  componentDidUpdate() {
    if (!this.state.animation_complete) {
      this.setState({animation_complete: true});
      this.setState({obfuscate: false});
      this.forceUpdate();
      if (this.props.callMethodTime) {
        this.parentMethod();
      }
    }
  }

  forceUpdate() {
    const { revealDuration, revealDelay } = this.props;
    setTimeout(() => {
      this.setState({obfuscate: false});
    }, revealDuration + revealDelay);
  }

  parentMethod() {
    const { callMethodTime } = this.props;
    setTimeout(() => {
      this.props.parentMethod();
    }, callMethodTime);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.animation_complete || this.state.force) {
      return false;
    } else {
      return true;
    }
  }

  render() {
    return(
      <span className="baffle_text">
        {this.text()}
      </span>
    );
  }

  text() {
    const { text, revealDuration, revealDelay } = this.props;
    if (!this.state.force && window.innerWidth > 670) {
        return (
          <Baffle
            speed={50}
            characters={this.props.characters ? this.props.characters : 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'}
            obfuscate={this.state.obfuscate}
            update={true}
            revealDuration={revealDuration}
            revealDelay={revealDelay}
          >
            {text}
          </Baffle>
        );      
    } else {
      return <span>{text}</span>;
    }
  }
}

export default handleViewport(BaffleText);
