import React from 'react';
import styled, { keyframes } from 'styled-components';
import styles from './Tabs.module.scss';
import Timeline from '../Timeline/Timeline';
import {FormattedMessage} from 'react-intl';

class Tabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: 'experience',
    };
  }

  render() {
    const ColorAnimation = keyframes`
            0%  {border-color: #04e5e5;}
            10% {border-color: #f37055;}
            20% {border-color: #ef4e7b;}
            30% {border-color: #a166ab;}
            40% {border-color: #5073b8;}
            50% {border-color: #04e5e5;}
            60% {border-color: #07b39b;}
            70% {border-color: #6fba82;}
            80% {border-color: #5073b8;}
            90% {border-color: #1098ad;}
            100% {border-color: #f37055;}
        `;

    const TabSelector = styled.button`
            color: #fff;
            font-size: 20px;
            font-weight: bold;
            background-color: transparent;
            border: none;
            margin: 0 10px 0 0;
            border-bottom: 2px solid #fff;
            transition: .5s;
            &:hover, &.active {
              animation: ${ColorAnimation} 10s infinite alternate;
            }
            &:focus {
              outline: none;
            }
            @media (max-width: 767px) {
              font-size: 18px;
            }
            @media (max-width: 370px) {
              font-size: 15px;
            }
        `;

    const Fade = keyframes`
            0% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        `;

    const Tab = styled.div`
            display: none;
            animation: ${Fade} 1s forwards;
        `;

    return(
      <div className={styles.tabContainer}>
        <div className={styles.tabSelectors}>
          <TabSelector className={this.state.tab === 'experience' ? ' active' : ''} onClick={() => this.setState({tab: 'experience'})}>
            <FormattedMessage id='tabExperience' />
          </TabSelector>
          <TabSelector className={this.state.tab === 'education' ? ' active' : ''} onClick={() => this.setState({tab: 'education'})}>
          <FormattedMessage id='tabEducation' />
          </TabSelector>
        </div>
        <div className={styles.tabs}>
          <Tab style={{display: this.state.tab === 'experience' ? 'block' : 'none'}}>
            <Timeline data={[
              {
                id: 1,
                year: <FormattedMessage id='swpsYear' />,
                title: <FormattedMessage id='swpsTitle' />,
                institution: <FormattedMessage id='swpsInstitution' />,
                description: <FormattedMessage id='swpsDescription' />,
              },
              {
                id: 2,
                year: <FormattedMessage id='assecoYear' />,
                title: <FormattedMessage id='assecoTitle' />,
                institution: <FormattedMessage id='assecoInstitution' />,
                description: <FormattedMessage id='assecoDescription' />,
              },
              {
                id: 3,
                year: <FormattedMessage id='mayerynYear' />,
                title: <FormattedMessage id='mayerynTitle' />,
                institution: <FormattedMessage id='mayerynInstitution' />,
                description: <FormattedMessage id='mayerynDescription' />,
              }]
            }
            />
          </Tab>
          <Tab style={{display: this.state.tab === 'education' ? 'block' : 'none'}}>
            <Timeline data={[
              {
                id: 1,
                year: <FormattedMessage id='webKodillaYear' />,
                title: <FormattedMessage id='webKodillaTitle' />,
                institution: <FormattedMessage id='webKodillaInstitution' />,
                description: <FormattedMessage id='webKodillaDescription' />,
              },
              {
                id: 2,
                year: <FormattedMessage id='javaKodillaYear' />,
                title: <FormattedMessage id='javaKodillaTitle' />,
                institution: <FormattedMessage id='javaKodillaInstitution' />,
                description: <FormattedMessage id='javaKodillaDescription' />,
              },
              {
                id: 3,
                year: <FormattedMessage id='politechnikaYear' />,
                title: <FormattedMessage id='politechnikaTitle' />,
                institution: <FormattedMessage id='politechnikaInstitution' />,
                description: <FormattedMessage id='politechnikaDescription' />,
              }]
            }
            />
          </Tab>
        </div>
      </div>
    );
  }
}

export default Tabs;
