import React from 'react';
import styles from './Glitch.module.scss';

class Glitch extends React.Component{
  render() {
    const {text} = this.props;

    return (
      <p className={styles.glitch} data-text={text}>{text}</p>
    );
  }
}

export default Glitch;
